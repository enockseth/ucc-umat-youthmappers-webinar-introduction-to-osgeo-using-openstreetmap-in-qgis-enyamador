# Enock Seth Nyamador

Presentation during UMaT and UCC YouthMappers webinar

Introduction to OSGeo and Using OpenStreetMap data in QGIS
Get to know the OSGeo; projects, community and using OpenStreetMap data in QGIS

Download presentation
[LINK](https://gitlab.com/enockseth/ucc-umat-youthmappers-webinar-introduction-to-osgeo-using-openstreetmap-in-qgis-enyamador/-/raw/master/Introduction%20to%20OSGeo%20and%20Uisng%20OpenStreetMap%20data%20in%20QGIS.pdf)
